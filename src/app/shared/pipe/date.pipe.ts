import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'momentPipe'
})
export class MomentPipe implements PipeTransform {

  transform(value, ...args: any[]): any {
    if (!value) {
      return value;
    }
    const [format] = args;
    return moment(value, 'YYYY-MM-DD').format(format);
  }
}
