import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {map} from 'rxjs/operators';
import {Customer} from '../customers/customer.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private firestore: AngularFirestore) {
  }

  getCustomers(requestParams?) {
    return this.firestore.collection('customers').snapshotChanges().pipe(
      map(customers => customers.map(a => {
        const customer = a.payload.doc.data();
        const id = a.payload.doc.id;
        return {id, ...customer};
      })));
  }

  addCustomer(customer: Customer) {
    this.firestore.collection('customers').add(customer);
  }

  updateCustomer(customer: Customer) {
    this.firestore.doc('customers/' + customer.id).update(customer).then((update) => {
      console.log(update);
    }, (error) => {
      console.warn('update failed', error);
    });
  }
  deleteCustomer(customerId: string) {
    this.firestore.doc('customers/' + customerId).delete().then((response) => {
      console.log(response);
    });
  }
}
