import { Injectable } from '@angular/core';
import {ApiService} from '../api/api.service';
import {Subject, Subscription} from 'rxjs';
import {Customer} from './customer.interface';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  customers$: Subject<Customer[]> = new Subject();
  customersSubscription: Subscription;
  constructor(private api: ApiService) { }
  getCustomers(requestParams?) {
    if (this.customersSubscription) {
      this.customersSubscription.unsubscribe();
    }
    this.customersSubscription = this.api.getCustomers(requestParams).subscribe((customers: Customer[]) => {
      this.customers$.next(customers);
    });
    return this.customers$.asObservable();
  }
  updateCustomer(customer) {
    this.api.updateCustomer(customer);
  }
  addCustomer(customer) {
    this.api.addCustomer(customer);
  }
  deleteCustomer(customerId) {
    this.api.deleteCustomer(customerId);
  }
}
