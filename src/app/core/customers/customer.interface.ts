export interface Customer {
  id: string;
  address: string;
  age: number;
  balance: number;
  company: string;
  email: string;
  gender: string;
  isActive: boolean;
  isVerified: boolean;
  lastTimeOnline?: string;
  name: string;
  phone: string;
  nextQuestDate?: string;
  questsComplete: number;
  questsRestarts: number;
}
