import {Injectable, NgZone} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {AngularFirestore} from '@angular/fire/firestore';
import {auth} from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: any;

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private router: Router, private ngZone: NgZone) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        console.log('user', user);
        this.user = user;
      } else {
        this.user = null;
        localStorage.removeItem('token');
        this.router.navigate(['/login']);
      }
    });
  }

  async login(email: string, password: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, password);
    this.router.navigate(['/home']);
  }

  logout() {
    this.afAuth.auth.signOut().then((success) => {
      localStorage.removeItem('token');
      this.router.navigate(['/login']);
    });
  }

  get isLoggedIn(): boolean {
    return !!this.user;
  }

  get token(): string {
    console.log('TOKEN !', localStorage.getItem('token'));
    return localStorage.getItem('token');
  }

  loginWithGoogle() {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider()).then((success) => {
      localStorage.setItem('token', success.credential['accessToken']);
      this.ngZone.run(() => {
        this.router.navigate(['/home']);
      });
    }, (error) => {
      console.log('login failed', error);
    });
  }
}
