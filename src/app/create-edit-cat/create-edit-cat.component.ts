import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import * as moment from 'moment';

@Component({
  selector: 'app-create-edit-cat',
  templateUrl: './create-edit-cat.component.html',
  styleUrls: ['./create-edit-cat.component.scss']
})
export class CreateEditCatComponent implements OnInit {
  fields: any[] = [];
  customerForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<CreateEditCatComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.init();
  }

  init() {
    const formConfig = {};
    this.fields = Object.keys(this.data.fields);
    for (const field in this.data.fields) {
      if (this.data.fields.hasOwnProperty(field)) {
        if (this.data.fields[field] === 'date') {
          formConfig[field] = [{
            value: this.data.customer ? this.data.customer[field] ? moment(this.data.customer[field], 'YYYY-MM-DD') : null : null,
            disabled: field === 'lastTimeOnline'
          }, Validators.required];
        } else if (this.data.fields[field] === 'boolean') {
          formConfig[field] = [this.data.customer ? this.data.customer[field] : false, Validators.required];
        } else {
          formConfig[field] = [{
            value: this.data.customer ? this.data.customer[field] : null,
            disabled: field === 'id'
          }, Validators.required];
        }
      }
    }
    console.warn('formConfig', formConfig);
    this.customerForm = this.formBuilder.group(formConfig);
  }

  submit() {
    if (this.customerForm.valid) {
      this.dialogRef.close(
        {
          customer: {
            ...this.data.customer,
            ...this.customerForm.value,
            ...{
              nextQuestDate: this.customerForm.value.nextQuestDate.format('YYYY-MM-DD')
            }
          },
          action: this.data.action
        }
      );
    }
  }

  cancel() {
    this.dialogRef.close();
  }

}
