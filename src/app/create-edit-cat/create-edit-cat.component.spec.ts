import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditCatComponent } from './create-edit-cat.component';

describe('CreateEditCatComponent', () => {
  let component: CreateEditCatComponent;
  let fixture: ComponentFixture<CreateEditCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEditCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
