import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../core/api/api.service';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {CreateEditCatComponent} from '../create-edit-cat/create-edit-cat.component';
import {customerTableColumns} from './customer-table-columns.conts';
import {CustomersService} from '../core/customers/customers.service';
import {customerFieldsTypes} from '../core/customers/customer.const';
import {AuthService} from '../core/auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  customers: any = [];
  tableColumnsToShow = customerTableColumns;
  displayColumns = {
    fields: [...customerTableColumns],
    map: {},
    closed: () => {
      this.tableColumnsToShow = this.displayColumns.fields.filter((field) => {
        return this.displayColumns.map[field];
      });
      console.log(this);
    }
  };

  constructor(public dialog: MatDialog, public customersService: CustomersService, public auth: AuthService) {
  }

  openDialog(action, customer = null): void {
    const fields = {...customerFieldsTypes};
    if (action === 'add') {
      delete fields.id;
      delete fields.lastTimeOnline;
    }
    const dialogRef = this.dialog.open(CreateEditCatComponent, {
      width: '500px',
      data: {customer, fields, action}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      if (result.action === 'add') {
        this.customersService.addCustomer(result.customer);
      } else {
          this.customersService.updateCustomer(result.customer);
      }
    });
  }

  ngOnInit() {
    const displayColumnsMap = {};
  //  this.tableColumnsToShow.splice(1, 0, 'id');
    this.displayColumns.fields.forEach((field) => {
      displayColumnsMap[field] = true;
    });
    this.displayColumns.map = displayColumnsMap;
    this.customersService.getCustomers().subscribe((customers) => {
      this.customers = new MatTableDataSource(customers);
      this.customers.sort = this.sort;
    });
  }

  deleteCustomer(customer) {
    this.customersService.deleteCustomer(customer.id);
  }

  applyFilter(filterValue) {
    this.customers.filter = filterValue.trim().toLowerCase();
  }

}
