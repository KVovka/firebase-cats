export const customerTableColumns = [
  'name',
  'gender',
  'age',
  'email',
  'isVerified',
  'isActive',
  'phone',
  'company',
  'balance',
  'address',
  'lastTimeOnline',
  'nextQuestDate',
  'questsComplete',
  'questsRestarts',
  'actions'
];
