// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyB5xQUYN61bCTJxWclfIv-3NsU9JFx37zs',
    authDomain: 'deep-firetest.firebaseapp.com',
    databaseURL: 'https://deep-firetest.firebaseio.com',
    projectId: 'deep-firetest',
    storageBucket: 'deep-firetest.appspot.com',
    messagingSenderId: '554534345513',
    appId: '1:554534345513:web:325c2765e5c3b4ab'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
